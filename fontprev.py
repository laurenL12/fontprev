#!/usr/bin/python3

import pyray as r
import sys
import string
import os
import ctypes
from raylib import ffi

if len(sys.argv) < 2:
    print('no font loaded')
    exit(1)

r.set_trace_log_level(r.LOG_NONE)

r.init_window(960, 640, 'fontprev')

fntpath = sys.argv[1]
NULL = ffi.cast('void *', 0)
fnt = r.load_font_ex(fntpath, 64, NULL, 95)
bgcolor = r.get_color(0x585266FF)

display_text = string.ascii_lowercase + '\n' + string.ascii_uppercase + '\n' \
        + string.digits + '\n' + string.punctuation

if os.path.exists(fntpath):
    r.set_window_title(f'fontprev - viewing {fntpath}')

r.set_target_fps(60)

while not r.window_should_close():
    r.begin_drawing()
    r.clear_background(bgcolor)
    if fnt.texture.id != r.get_font_default().texture.id:
        r.draw_text_ex(fnt, display_text, (10, 10), 30, 2, r.WHITE)
    else:
        r.draw_text(f"could not load font '{fntpath}'", 10, 10, 20, r.WHITE)

    r.end_drawing()

r.unload_font(fnt)
r.close_window()
