# fontprev
Silly project to help on font visualization

**Warning: you may only load fonts that support ASCII. Fonts with other special characters may not be loaded properly.**

## Running
```
$ git clone https://gitlab.com/laurenL12/fontprev.git
$ cd fontprev
$ pip install -r requirements.txt
$ python3 fontprev.py <font>
```
